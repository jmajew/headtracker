/************************************************************
MPU9250_DMP_Quaternion
 Quaternion example for MPU-9250 DMP Arduino Library 
Jim Lindblom @ SparkFun Electronics
original creation date: November 23, 2016
https://github.com/sparkfun/SparkFun_MPU9250_DMP_Arduino_Library

The MPU-9250's digital motion processor (DMP) can calculate
four unit quaternions, which can be used to represent the
rotation of an object.

This exmaple demonstrates how to configure the DMP to 
calculate quaternions, and prints them out to the serial
monitor. It also calculates pitch, roll, and yaw from those
values.

Development environment specifics:
Arduino IDE 1.6.12
SparkFun 9DoF Razor IMU M0

Supported Platforms:
- ATSAMD21 (Arduino Zero, SparkFun SAMD21 Breakouts)
*************************************************************/
#include <SparkFunMPU9250-DMP.h>
#include "Joystick.h"

#define SerialPort SerialUSB

// const byte pinInt = 4;
const byte pinButton = 7;
const byte pinLed = 13;
int ledcount = 0;

const uint8_t maxcount = 20;
const float maxpitch = 20.0f;
const float maxroll = 20.0f;
const float maxyaw = 20.0f;
const int maxJoy = 127;


const float expo_coeff = 1.5f;
const float w = 0.2f;

float pitch, roll, yaw;
float pitch0 = 0.0f, roll0 = 0.0f, yaw0 = 0.0f;
float fpitch = 0.0f, froll = 0.0f, fyaw = 0.0f;
int8_t joyX, joyY, joyZ;

char strt[16];

MPU9250_DMP imu;


inline float angleNorm( float a)
{
	return a > 180.0f ? a - 360.0f : ( a < -180.0f ? a + 360.0f : a) ;
}


inline float clamp( float v, float maxv)
{
	return max( min( v, maxv), -maxv);
}


inline float expo( float x, float y)
{
	if ( x >= 0 )
	{
		return x > 1.0f ? 1.0f : pow(x,y) ;
	}
	else
	{
		x = -x;
		return - (x > 1.0f ? 1.0f : pow(x,y) );
	}
}

inline int8_t pitchToJoy( float a)
{
	return (int8_t)( (float)maxJoy * expo( clamp( a, maxpitch) / maxpitch, expo_coeff) );
}

inline int8_t rollToJoy( float a)
{
	return (int8_t)( (float)maxJoy * expo( clamp( a, maxroll) / maxroll, expo_coeff) );
}

inline int8_t yawToJoy( float a)
{
	return (int8_t)( (float)maxJoy * expo( clamp( a, maxyaw) / maxyaw, expo_coeff) );
}







void setup() 
{
	SerialPort.begin(115200);

	// Call imu.begin() to verify communication and initialize
	if (imu.begin() != INV_SUCCESS)
	{
		while (1)
		{
			SerialPort.println("Unable to communicate with MPU-9250");
			SerialPort.println("Check connections, and try again.");
			SerialPort.println();
			delay(1000);
		}
	}

	imu.dmpBegin( DMP_FEATURE_6X_LP_QUAT | 	// Enable 6-axis quat
				  DMP_FEATURE_GYRO_CAL, 	// Use gyro calibration
				  50); 						// Set DMP FIFO rate to 10 Hz
											// DMP_FEATURE_LP_QUAT can also be used. It uses the 
											// accelerometer in low-power mode to estimate quat's.
											// DMP_FEATURE_LP_QUAT and 6X_LP_QUAT are mutually exclusive
  
	pinMode( pinButton, INPUT_PULLUP);    
	pinMode( pinLed, OUTPUT);    
	digitalWrite( pinLed, HIGH);
 	
	Joystick.begin(false); 
	
	setZeroVals();
	printZero();
}



void loop() 
{
	if ( digitalRead( pinButton) == LOW )
	{
		
		setZeroVals();
		printZero();
		delay(200);
	}
	
	if ( imu.fifoAvailable() )
	{
		if ( imu.dmpUpdateFifo() == INV_SUCCESS)
		{
			imu.computeEulerAngles();
			
			// imu.updateCompass();
			// float head = imu.computeCompassHeading();
			// SerialPort.println( head);

			pitch = angleNorm( angleNorm( imu.pitch) - pitch0 );
			roll  = angleNorm( angleNorm( imu.roll) - roll0);
			yaw   = angleNorm( angleNorm( imu.yaw) - yaw0);

			setJoystick();

			//SerialPort.print("R/P/Y: " + String(imu.roll) + ", " + String(imu.pitch) + ", " + String(imu.yaw));
			SerialPort.print(" time=" + String(imu.time) + "  R/P/Y: ");
			dtostrf(roll, 8, 1, strt);		
			Serial.print(strt);      
			dtostrf(pitch, 8, 1, strt);		
			Serial.print(strt);      
			dtostrf(yaw, 8, 1, strt);		
			Serial.print(strt);      
			SerialPort.print("  jX/jY/jZ: " + String(joyX) + ", " + String(joyY) + ", " + String(joyZ));
			
			SerialPort.println();
			
			if ( (++ledcount ) % 10 == 0 )
			{
				ledcount = 0;
				digitalWrite( pinLed, !digitalRead( pinLed));
			}
		}
	}
	
}


void setZeroVals()
{
	SerialPort.println("ZERO=");
	
	uint8_t count = 0;
	pitch0 = yaw0 = roll0 = 0.0;
	do
	{
		if ( imu.fifoAvailable() )
		{
			if ( imu.dmpUpdateFifo() == INV_SUCCESS)
			{
				imu.computeEulerAngles();

				float pitchc = angleNorm( angleNorm( imu.pitch));
				float rollc  = angleNorm( angleNorm( imu.roll));
				float yawc   = angleNorm( angleNorm( imu.yaw));

				SerialPort.println("R/P/Y: " + String(rollc) + ", " + String(pitchc) + ", " + String(yawc));
				
				pitch0 += pitchc;
				roll0 += rollc;
				yaw0 += yawc;

				++count;
			}
		}		
	}
	while ( count < maxcount);

	SerialPort.println("zero = R/P/Y: " + String(roll0) + ", " + String(pitch0) + ", " + String(yaw0));
	
	pitch0 /= count;
	roll0 /= count;
	yaw0 /= count;

	SerialPort.println("zero = R/P/Y: " + String(roll0) + ", " + String(pitch0) + ", " + String(yaw0));
	
	yaw = yaw0;
	pitch = pitch0;
	roll = roll0;
}


void printZero()
{
	SerialPort.print("pitch0 = ");
	SerialPort.print(pitch0);
	SerialPort.print("roll0 = ");
	SerialPort.print(roll0);
	SerialPort.print("yaw0 = ");
	SerialPort.print(yaw0);
	SerialPort.println();
	SerialPort.println();
}


void setJoystick()
{
	fpitch = (1-w)* fpitch + w* pitch;
	froll  = (1-w)* froll  + w* roll;
	fyaw   = (1-w)* fyaw   + w* yaw;

	joyX = pitchToJoy( fpitch);
	joyY = rollToJoy( froll);
	joyZ = yawToJoy( fyaw);
	
	Joystick.setXAxis( joyX);
	Joystick.setYAxis( joyY);
	Joystick.setZAxis( joyZ);
	Joystick.sendState();
}



void printIMUData(void)
{  
  // After calling dmpUpdateFifo() the ax, gx, mx, etc. values
  // are all updated.
  // Quaternion values are, by default, stored in Q30 long
  // format. calcQuat turns them into a float between -1 and 1
  float q0 = imu.calcQuat(imu.qw);
  float q1 = imu.calcQuat(imu.qx);
  float q2 = imu.calcQuat(imu.qy);
  float q3 = imu.calcQuat(imu.qz);

  SerialPort.println("Q: " + String(q0, 4) + ", " +
                    String(q1, 4) + ", " + String(q2, 4) + 
                    ", " + String(q3, 4));
  SerialPort.println("R/P/Y: " + String(imu.roll) + ", "
            + String(imu.pitch) + ", " + String(imu.yaw));
  SerialPort.println("Time: " + String(imu.time) + " ms");
  SerialPort.println();
}

