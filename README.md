## HeadTracker

Software tracks head movements and pass them out to the pc as a virtual joystick device.
Tested using Mpu9250 connected to the Pololu A-Star32U4 (should work with Arduino Leonardo like boards) 
and the device was mounted on top of the earphones.
The soft is using Mpu9250 internal prcoessor (DMP) for computing position quaternion 
which is converted to the Euler angles.
Gyro bias is calculated assuming that the head, most of the time, is not moving (it takes few minutes to get 
good approximation). The button is used to reset initial position in case the drift is too big.

Depends on 2 libraries:

* [Joystick](https://github.com/MHeironimus/ArduinoJoystickLibrary/tree/version-1.0)

* modified [SparkFun MPU-9250 Digital Motion Processor (DMP) Arduino Library](https://github.com/sparkfun/SparkFun_MPU-9250-DMP_Arduino_Library)

### hook-up guide

MPU9250 SCL -> pin 3

MPU9250 SDA -> pin 2

button -> pin 4 and GND (used for zero-ing the tracker position)
