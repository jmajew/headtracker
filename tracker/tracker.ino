#include "MPU9250.h"
#include "eeprom_utils.h"
#include "Joystick.h"

//#define DEBUG


const byte pinInt = 4;
const byte pinButton = 7;
const byte pinLed = 13;


MPU9250 mpu;
//class MPU9250_<TwoWire, AFS::A4G, GFS::G500DPS, MFS::M14BITS> mpu;

enum
{
	MODE_UNKNOWN = 0,
	MODE_CALIB = 1,
	MODE_RUN = 2,
};


byte mode = MODE_UNKNOWN;


const uint8_t maxcount = 100;
const float maxpitch = 10.0;
const float maxroll = 10.0;
const float maxyaw = 20.0;
const int maxJoy = 127;


const float expo_coeff = 1.5;
const float w = 0.2;
const float maxdrift = 5;

float pitch0 = 0.0;
float roll0 = 0.0;
float yaw0 = 0.0;


float pitch;
float roll;
float yaw;
//float yawp, rollp, pitchp;

uint32_t time = 0, timep = 0;

inline float clamp( float v, float maxv)
{
	return max( min( v, maxv), -maxv);
}


inline float expo( float x, float y)
{
	if ( x >= 0 )
	{
		return x > 1.0f ? 1.0f : pow(x,y) ;
	}
	else
	{
		x = -x;
		return - (x > 1.0f ? 1.0f : pow(x,y) );
	}
}

inline int8_t pitchToJoy( float a)
{
	return (int8_t)( (float)maxJoy * expo( clamp(a - pitch0, maxpitch) / maxpitch, expo_coeff) );
}

inline int8_t rollToJoy( float a)
{
	return (int8_t)( (float)maxJoy * expo( clamp(a - roll0, maxroll) / maxroll, expo_coeff) );
}

inline int8_t yawToJoy( float a)
{
	return (int8_t)( (float)maxJoy * expo( clamp(a - yaw0, maxyaw) / maxyaw, expo_coeff) );
}



void setZeroVals()
{
	uint8_t count = 0;
	pitch0 = yaw0 = roll0 = 0.0;
	do
	{
		if ( digitalRead(pinInt) == LOW )
		{
			mpu.update();
			
			pitch0 += mpu.getPitch();
			roll0 += mpu.getRoll();
			yaw0 += mpu.getYaw();
			
			++count;
		}
	}
	while ( count < maxcount);
	
	pitch0 /= count;
	roll0 /= count;
	yaw0 /= count;
	
	yaw = yaw0;
	pitch = pitch0;
	roll = roll0;
}

void setup()
{
    Serial.begin(115200);

	Wire.begin();

	delay(2000);
	mpu.setup();
	
	delay(5000);

	// // calibrate when you want to
	// mpu.calibrateAccelGyro();
	// mpu.calibrateMag();

	// // save to eeprom
	// saveCalibration();

	pinMode( pinInt, INPUT_PULLUP);    
	pinMode( pinButton, INPUT_PULLUP);    
	pinMode( pinLed, OUTPUT);    

	// // load from eeprom
	// loadCalibration();
	// mpu.printCalibration();

	//setZeroVals();
	Joystick.begin(false);

}

char strt[16];
byte k = 0;

void loop()
{
	// if ( mode == MODE_CALIB )
	// {
		// digitalWrite(pinLed, HIGH);
		// delay(200);                     
		// digitalWrite(pinLed, LOW);
		// delay(100);                    
		// return;
	// }
	
	if ( digitalRead( pinButton) == LOW )
	{
		setZeroVals();
		delay(500);
	}
	
    if ( digitalRead(pinInt) == LOW )
    {
		digitalWrite(pinLed, HIGH);
		
        mpu.update();
        //mpu.print();

    
		// yawp = yaw;
		// yaw = mpu.getYaw();
		
		// timep = time;
		// time = micros();
		// uint32_t dtime = time - timep;
		// float dydt = (yaw-yawp)*1.0e6f / (float)dtime;
		
		// if ( fabs(dydt) < maxdrift)
		// {
			// yaw0 += yaw - yawp;
		// }

		// yawp = yaw;
		// pitchp = pitch;
		// rollp = roll;
		pitch = (1-w)* pitch + w* mpu.getPitch();
		roll  = (1-w)* roll  + w* mpu.getRoll();
		yaw   = (1-w)* yaw   + w* mpu.getYaw();

		int8_t joyX = pitchToJoy( pitch);
		int8_t joyY = rollToJoy( roll);
		int8_t joyZ = yawToJoy( mpu.getYaw());
		
		Joystick.setXAxis( joyX);
		Joystick.setYAxis( joyY);
		Joystick.setZAxis( joyZ);
		Joystick.sendState();


#ifdef DEBUG    
	    Serial.print("roll, pitch, yaw : mag =  ");
		dtostrf(mpu.getRoll(), 8, 1, strt);		
		Serial.print(strt);
        // Serial.print(" : ");
		
		dtostrf(mpu.getPitch(), 8, 1, strt);		
		Serial.print(strt);
        // Serial.print(" : ");

		dtostrf(mpu.getYaw(), 8, 1, strt);		
		Serial.print(strt);      
        
        Serial.print(" : ");
		
		for ( int i=0; i<3; ++i)
		{
			// dtostrf(mpu.getMag(i), 8, 1, strt);		
			// Serial.print(strt);      
			Serial.print(mpu.getMag(i));      
			Serial.print(" ");      
		}
			
		
		
        // Serial.print("roll, pitch, yaw =  ");
        // Serial.print(mpu.getRoll(),1);
        // Serial.print(" : ");
        // Serial.print(mpu.getPitch(),1);
        // Serial.print(" : ");
        // Serial.print(mpu.getYaw(),1);

        //Serial.print( dydt);
        Serial.println();
        
        //Serial.println( "ok");
#endif
        

//        prev_ms = millis();
    }
}
