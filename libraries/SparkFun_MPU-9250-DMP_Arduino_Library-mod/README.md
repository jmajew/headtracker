
Original [SparkFun MPU-9250 Digital Motion Processor (DMP) Arduino Library](https://github.com/sparkfun/SparkFun_MPU-9250-DMP_Arduino_Library) modified for Atmega-32U4 uC.

Change list:

* Added modification proposed in [post](https://github.com/sparkfun/SparkFun_MPU-9250-DMP_Arduino_Library/issues/11#issuecomment-449472273). 
Thanks to that, memory of the 32U4 is enough to handle loading the dmp firmware

* Conversion from 30bit integer to float

* Euler angles calculations
